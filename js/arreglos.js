// Declaración de un arreglo
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9];

// Función para mostrar los elementos del arreglo
function mostrarArray() {
    for (let con = 0; con < arreglo.length; ++con) {
        console.log(con + ": " + arreglo[con]);
    }
}

// Función que regresa el promedio de los elementos del arreglo
function promedio() {
    let pro = 0;
    for (con = 0; con < arreglo.length; ++con) {
        pro += arreglo[con];
    }
    pro = pro / arreglo.length;
    return pro;
}

// Función para contar números pares e impares
function pares() {
    let contador = 0;
    let contador2 = 0;
    for ( con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] % 2 === 0) {
            contador++;
        } else {
            contador2++;
        }
    }
    console.log("Los pares son: " + contador);
    console.log("Los impares son: " + contador2);
}

function mayor() {
    let mayor = arreglo[0];
    for (con = 1; con < arreglo.length; ++con) {
        if (arreglo[con] > mayor) {
            mayor = arreglo[con];
        }
    }
    console.log("El número mayor es: " + mayor);
}

function aleatorioarray(){
    let num = parseInt(prompt("Por favor, ingresa un número:"));
    let arreglo6 = new Array(num);
    if (!isNaN(num)) {
        for(let i=0;i<num;i++){
            arreglo6[i]=Math.floor(Math.random()*1000);
        }
    } else {
        console.log("No ingresaste un número válido.");
    }
    console.log("El arreglo es:"+arreglo6);
    }

    function ramdomze(x) {
        let arr = new Array(x);
        const cmbAleatorios = document.getElementById("cmbNumeros");
        for (let con = 0; con < x; con++) {
            arr[con] = Math.floor(Math.random() * 1000);
            let option = document.createElement("option");
            option.value = arr[con];
            option.innerHTML = arr[con];
            cmbAleatorios.appendChild(option);
        }
    }
    
    


// Llamada a las funciones
ramdomze(10);
aleatorioarray();
pares();
mayor();
mostrarArray();
const prom = promedio();
console.log("El Promedio es: " + prom);
