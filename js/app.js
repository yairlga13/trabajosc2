// Obtener el objeto botton de calcular
const btnCalcular = document.getElementById("btnCalcular");
btnCalcular.addEventListener("click", function() {
    let valorAuto = parseFloat(document.getElementById("valorAuto").value);
    let porcentaje = parseFloat(document.getElementById("txtPorcentaje").value);
    let plazo = parseFloat(document.getElementById("plazo").value);

    // HACER LOS CALCULOS
    let txtPagoInicial = valorAuto * (porcentaje / 100);
    let totalFin = valorAuto - txtPagoInicial;
    let pagoMensual = totalFin / plazo;

    // MOSTRAR LOS DATOS
    document.getElementById("pagoInicial").value = txtPagoInicial;
    document.getElementById("TotalFin").value = totalFin;
    document.getElementById("pagoMensual").value = pagoMensual;
});

// Agregar función para el botón "Limpiar" si es necesario
const btnLimpiar = document.getElementById("btnLimpiar");
btnLimpiar.addEventListener("click", function() {
    document.getElementById("valorAuto").value = "";
    document.getElementById("txtPorcentaje").value = "";
    document.getElementById("plazo").value = "12"; 
    document.getElementById("pagoInicial").value = "";
    document.getElementById("TotalFin").value = "";
    document.getElementById("pagoMensual").value = "";
});
