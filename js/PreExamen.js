const selectNumero = document.getElementById('numero');
const tablaMultiplicar = document.getElementById('tabla-multiplicar');
const mostrarTablaButton = document.getElementById('mostrar-tabla');

// Inicialmente, oculta la tabla
tablaMultiplicar.style.display = 'none';

selectNumero.addEventListener('change', (event) => {
    numeroSeleccionado = event.target.value;
  });
mostrarTablaButton.addEventListener('click', mostrarTabla);

function mostrarTabla() {
  const numeroSeleccionado = selectNumero.value;
  let tablaHTML = '<h2>Tabla de Multiplicar del ' + numeroSeleccionado + '</h2>';
  for (let i = 1; i <= 10; i++) {
    const resultado = numeroSeleccionado * i;
    // Utiliza imágenes para mostrar los números y los signos
    tablaHTML += `<img src="/img/${numeroSeleccionado}.png" alt="${numeroSeleccionado}" />`; // Muestra el número
    tablaHTML += `<img src="/img/x.png" alt="x" />`; // Muestra el signo de multiplicación
    tablaHTML += `<img src="/img/${i}.png" alt="${i}" />`; // Muestra el segundo número
    tablaHTML += `<img src="/img/=.png" alt="=" />`; // Muestra el signo de igual
    if(resultado<=10){
    tablaHTML += `<img src="/img/${resultado}.png" alt="${resultado}" />`; // Muestra el resultado
    
    }else if(resultado>10){
        const digitos = resultado.toString().split('');
    for (const d of digitos) {
      tablaHTML += `<img src="/img/${d}.png" alt="${d}" />`; // Muestra cada dígito
    }
    }
    tablaHTML += '<br>';
  }
  tablaMultiplicar.innerHTML = tablaHTML;
 
    tablaMultiplicar.style.display = 'block';

}
