// Obtener el objeto botón de calcular
const btnCalcularimc = document.getElementById("btnCalcularimc");
btnCalcularimc.addEventListener("click", function() {
    const Edad = parseFloat(document.getElementById("Edad").value);
    const Altura = parseFloat(document.getElementById("Altura").value);
    const Peso = parseFloat(document.getElementById("Peso").value);

    // Hacer los cálculos
    const resultado = Peso / (Altura ** 2);
    let RCalorias;
    

    // Mostrar los datos
    document.getElementById("resultadoIMC").textContent = "Tu IMC es de: " + resultado.toFixed(2);
    
    // Resultados en imagen
    let imagen = document.getElementById("imagenCambiante");

    const sexoMasculino = document.getElementById("sexoMasculino");
    const sexoFemenino = document.getElementById("sexoFemenino");

    if (sexoMasculino.checked) {
        if (resultado < 18.5) {
            imagen.src = "/img/01H.png";
        } else if (resultado >= 18.5 && resultado <= 24.9) {
            imagen.src = "/img/02H.png";
        } else if (resultado >= 25 && resultado <= 29.9) {
            imagen.src = "/img/03H.png";
        } else if (resultado >= 30 && resultado <= 34.9) {
            imagen.src = "/img/04H.png";
        } else if (resultado >= 35 && resultado <= 39.9) {
            imagen.src = "/img/05H.png";
        } else if (resultado >= 40) {
            imagen.src = "/img/06H.png";
        }
        if(Edad >=10 && Edad <18){
            RCalorias= 17.686 * Peso + 658.2;
        }else  if(Edad >=18 && Edad <30){
            RCalorias= 15.057 * Peso + 692.2;
        }else  if(Edad >=30 && Edad <60){
            RCalorias= 11.472 * Peso + 873.1;
        }else  if(Edad >=60 ){
            RCalorias= 11.711 * Peso + 587.7;
        }
        document.getElementById("Calorias").textContent = "Debes consumir " + RCalorias.toFixed(2)+" Calorias";
    } else if (sexoFemenino.checked) {
        if (resultado < 18.5) {
            imagen.src = "/img/01M.png";
        } else if (resultado >= 18.5 && resultado <= 24.9) {
            imagen.src = "/img/02M.png";
        } else if (resultado >= 25 && resultado <= 29.9) {
            imagen.src = "/img/03M.png";
        } else if (resultado >= 30 && resultado <= 34.9) {
            imagen.src = "/img/04M.png";
        } else if (resultado >= 35 && resultado <= 39.9) {
            imagen.src = "/img/05M.png";
        } else if (resultado >= 40) {
            imagen.src = "/img/06M.png";
        }
        if(Edad >=10 && Edad <18){
            RCalorias= 13.384 * Peso + 692.6;
        }else  if(Edad >=18 && Edad <30){
            RCalorias= 14.818 * Peso + 486.6;
        }else  if(Edad >=30 && Edad <60){
            RCalorias= 8.126 * Peso + 845.6;
        }else  if(Edad >=60 ){
            RCalorias= 9.082 * Peso + 658.5;
        }
        document.getElementById("Calorias").textContent = "Debes consimir " + RCalorias.toFixed(2)+" Calorias";
    }
    
});

