const generarButton = document.getElementById("Gen");
const mostrarGeneradosSelect = document.getElementById("mosgen");

generarButton.addEventListener("click", () => {
    const cantidadGenerar = parseInt(document.getElementById("numGen").value);
    const numerosAleatorios = generarNumerosAleatorios(cantidadGenerar);

    // Mostrar números generados en el <select>
    mostrarNumerosGenerados(numerosAleatorios);

    // Mostrar números pares en el label correspondiente
    mostrarNumerosPares(numerosAleatorios);

    // Mostrar valor mayor y su posición en el label correspondiente
    mostrarValorMayor(numerosAleatorios);

    // Mostrar promedio en el label correspondiente
    mostrarPromedio(numerosAleatorios);

    // Mostrar valor menor y su posición en el label correspondiente
    mostrarValorMenor(numerosAleatorios);

    // Mostrar porcentaje de simetría en el label correspondiente
    mostrarPorcentajeSimetria(numerosAleatorios);
});

function generarNumerosAleatorios(cantidad) {
    const numeros = [];
    for (let i = 0; i < cantidad; i++) {
        numeros.push(Math.floor(Math.random() * 1000));
    }
    return numeros;
}

function mostrarNumerosGenerados(numeros) {
    mostrarGeneradosSelect.innerHTML = "";
    for (const numero of numeros) {
        const option = document.createElement("option");
        option.value = numero;
        option.textContent = numero;
        mostrarGeneradosSelect.appendChild(option);
    }
}

function mostrarNumerosPares(numeros) {
    const pares = numeros.filter(numero => numero % 2 === 0);
    const paresLabel = document.getElementById("pares");
    paresLabel.textContent = `Números Pares: ${pares.join(", ")}`;
}

function mostrarValorMayor(numeros) {
    const max = Math.max(...numeros);
    const index = numeros.indexOf(max);
    const valorMayorLabel = document.getElementById("valorMayor");
    valorMayorLabel.textContent = `Valor Mayor: ${max}, Posición: ${index}`;
}

function mostrarPromedio(numeros) {
    const promedio = numeros.reduce((acc, numero) => acc + numero, 0) / numeros.length;
    const promedioLabel = document.getElementById("promedio");
    promedioLabel.textContent = `Promedio: ${promedio}`;
}

function mostrarValorMenor(numeros) {
    const min = Math.min(...numeros);
    const index = numeros.indexOf(min);
    const valorMenorLabel = document.getElementById("valorMenor");
    valorMenorLabel.textContent = `Valor Menor: ${min}, Posición: ${index}`;
}

function mostrarPorcentajeSimetria(numeros) {
    const sum = numeros.reduce((acc, numero) => acc + numero, 0);
    const promedio = sum / numeros.length;
    const porcentaje = (promedio / 500) * 100; // Suponiendo que 500 es el valor medio.
    const porcentajeLabel = document.getElementById("porcentaje");
    porcentajeLabel.textContent = `Porcentaje de Simetría: ${porcentaje.toFixed(2)}%`;
}
